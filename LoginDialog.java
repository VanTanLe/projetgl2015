import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class LoginDialog extends JDialog{

	private JTextField tfNom;
	private JTextField tfPrenom;
	private JLabel lbNom;
	private JLabel lbPrenom;
	private JButton bouttonLogin;
	private boolean succeeded;
	private JButton btnCancel;
	
	public LoginDialog(Frame parent){
		super(parent,"Login",true);
		
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints cs = new GridBagConstraints();
		
		lbNom = new JLabel("Nom : ");
		cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lbNom, cs);
		
		tfNom = new JTextField(20);
		cs.gridx = 1;
		cs.gridy = 0;
        cs.gridwidth = 2;
        panel.add(tfNom, cs);
		
		lbPrenom = new JLabel("Prenom : ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lbPrenom, cs);
		
		tfPrenom = new JTextField(20);
		cs.gridx = 1;
		cs.gridy = 1;
		cs.gridwidth = 2;
		panel.add(tfPrenom,cs);
		panel.setBorder(new LineBorder(Color.GRAY));
		
		bouttonLogin = new JButton("Se connecter");
		
		bouttonLogin.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (Login.authenticate(getNom(),getPrenom())){
					JOptionPane.showMessageDialog(LoginDialog.this,
						"Bonjour " + getNom() + " " + getPrenom(),
						"Login",
						JOptionPane.INFORMATION_MESSAGE);
					succeeded = true;
					dispose();
				}else{
					JOptionPane.showMessageDialog(LoginDialog.this,
						"Moniteur introuvable",
						"Login",
						JOptionPane.ERROR_MESSAGE);
						tfNom.setText("");
						tfPrenom.setText("");
						succeeded = false;
				}
			}
		});
		btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(bouttonLogin);
        bp.add(btnCancel);
 
        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);
 
        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
	
	public String getNom(){
		return tfNom.getText().trim();
	}
	
	public String getPrenom(){
		return tfPrenom.getText().trim();
	}
	
	public boolean isSucceeded(){
		return succeeded;
		}
}
	
	
	
	
 
					
					
					
					
		
		
		
		
		
		
		
		
